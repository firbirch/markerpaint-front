if ($('.js-faq-wrap').length > 0) {
    const ScrollMagic = require('scrollmagic');

    const $nav = () => $('.js-faq-nav', '.js-faq-wrap');

    let scrollDuration = () => $('.js-faq-wrap').outerHeight() - $nav().outerHeight();

    const controller = new ScrollMagic.Controller();

    const scrollFaqNav = () => {

        const scene = new ScrollMagic.Scene({
                triggerElement: '.js-faq-wrap',
                triggerHook: 0,
                duration: scrollDuration(),
                offset: -135
            })
            .setPin($nav()[0])
            .addTo(controller);

        return scene;
    };


    let scene;

    if ($(window).outerWidth() > 991) {
        scene = scrollFaqNav();
    }

    let resizeEvt;

    $(window).bind('resize.faqScroll', function () {
        clearTimeout(resizeEvt);
        resizeEvt = setTimeout((function () {

            if ($(window).outerWidth() > 991) {

                if (scene !== undefined) {
                    //Update scroll duration
                    scene.duration(scrollDuration());
                } else {
                    //Initialize
                    scene = scrollFaqNav();
                }
            } else if (scene !== undefined) {
                scene.destroy(true);
                scene = undefined;
            }


        }), 500);

    });
}


/*========================================================
=            Подсветка якорей на странице FAQ            =
========================================================*/

var faqMenu = $(".js-faq-nav"),
    faqMenuHeight = faqMenu.outerHeight() + 15,
    // All list items
    faqMenuItems = faqMenu.find("a"),
    // Anchors corresponding to menu items
    faqScrollItems = faqMenuItems.map(function () {
        let item = $($(this).attr("href"));
        if (item.length) {
            return item;
        }
    });

$(window).scroll(function () {
    let fromTop = $(this).scrollTop() + faqMenuHeight;

    let cur = faqScrollItems.map(function () {
        if ($(this).offset().top < fromTop)
            return this;
    });
    cur = cur[cur.length - 1];
    let id = cur && cur.length ? cur[0].id : '';
    faqMenuItems
        .parent().removeClass("_active")
        .end().filter("[href='#" + id + "']").parent().addClass("_active");
});

/*=====  End of Подсветка якорей на странице FAQ  ======*/
let settings = {
    closeExisting: false,
    keyboard: true,
    arrows: true,
    buttons: [
        "zoom",
        "slideShow",
        "thumbs",
        "close"
    ]
}

$('.js-projects-fancybox').each(function() {
    $(this).click(function() {
        let gallery = $(this).closest('.projects__item').find('.js-fancybox-gallery-item');
        $.fancybox.defaults.btnTpl.counter = '<div><span data-fancybox-index></span> / <span data-fancybox-count></span></div>';
        $.fancybox.open(gallery, {
            
            keyboard: true,
            loop: true,
            toolbar: true,
            arrows  : false,
            infobar : false,
            margin  : [44,0,22,0],
            buttons : [
                'arrowLeft',
                'counter',
                'arrowRight',
                'close'
            ],
            thumbs: {
                autoStart: true,
                axis: 'x'
            }
        });
    })
});

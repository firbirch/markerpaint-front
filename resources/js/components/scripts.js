/*======================================================================
=            Функционал формы на странице оформление заказа            =
======================================================================*/
const deliveryMoscowToggle = $('.order__data-town-toggle[data-town="moscow"]');
const deliveryRegionsToggle = $('.order__data-town-toggle[data-town="regions"]');
const deliveryMoscow = $('.order__data-delivery-moscow');
const deliveryRegions = $('.order__data-delivery-regions');

deliveryMoscowToggle.click(function (e) {
  e.preventDefault();
  deliveryMoscow.show();
  deliveryRegions.hide();
  deliveryRegionsToggle.removeClass('order__data-town-toggle--active');
  deliveryMoscowToggle.addClass('order__data-town-toggle--active');
});

deliveryRegionsToggle.click(function (e) {
  e.preventDefault();
  deliveryRegions.show();
  deliveryMoscow.hide();
  deliveryMoscowToggle.removeClass('order__data-town-toggle--active');
  deliveryRegionsToggle.addClass('order__data-town-toggle--active');
});

const delivery = $('.order__data-item_delivery');
const deliveryRadio = delivery.find('.order__data-item-radiobtns input');
const defaultDeliveryindow = delivery.find('.order__data-item_delivery-info-default');
const pickupDeliveryindow = delivery.find('.order__data-item_delivery-info-pickup');
const deliveryRegionsFrom = $('.js-delivery-regions-form');
const deliveryRegionsMapWrap = $('.js-delivery-regions-map');
const deliveryRegionsMap = $('.order__data-item-map');

deliveryRadio.on('change', function () {
  var th = $(this);

  if (th.attr("data-val") === 'pickup') {
    defaultDeliveryindow.addClass('minimized');
    pickupDeliveryindow.removeClass('minimized');
    defaultDeliveryindow.find('.form-control').addClass('ignore');
  } else if (th.attr("data-val") === 'delivery') {
    defaultDeliveryindow.removeClass('minimized');
    pickupDeliveryindow.addClass('minimized');
    defaultDeliveryindow.find('.form-control').removeClass('ignore');
  } else if (th.attr("data-val") === 'pickup-regions') {
    deliveryRegionsFrom.hide();
    deliveryRegionsMapWrap.show();
    deliveryRegionsMap.removeClass('hidden');
  } else if (th.attr("data-val") === 'delivery-regions') {
    deliveryRegionsFrom.show();
    deliveryRegionsMapWrap.hide();
    deliveryRegionsMap.addClass('hidden');
  }
});

const $paymentTabs = $('.js-order-payment-tabs');

$paymentTabs.each(function () {
  const $self = $(this);
  const $tab = $('.js-order-payment-tab', $self);
  const $content = $('.js-order-payment-content', $self);

  $tab.on('change', function () {
    const val = $(this).data('val');
    $content.removeClass('_show').filter((index, item) => $(item).data('target') === val).addClass('_show');
  })
});


/*=====  End of Функционал формы на странице оформление заказа  ======*/

/*==============================================================
=            Изменение текста при загрузке файла               =
==============================================================*/
let fileInput = $('.ordering-block-download__input');
let fileLabelText = $('.ordering-block-download__field-text');

fileInput.change(function () {
  fileLabelText.text('Реквизиты ' + fileInput.val().replace(/.*\\/, ""));
});
/*=====  	Изменение текста при загрузке файла  		======*/

/*=======================================================
=            Скролл к якорям на странице FAQ            =
=======================================================*/

$('body').on('click', '.faq__nav-list a', function (e) {
  e.preventDefault();

  var th = $(this),
    target = $(th.attr('href'));

  if (target.length) {
    scrollToSection(target, 25);
  }
});

function scrollToSection(target, offset = 0) {
  var scrollTo = target.offset().top - offset;

  $('body, html').animate({
    scrollTop: scrollTo
  }, 500);
};

/*=====  End of Скролл к якорям на странице FAQ  ======*/







/*==================================================================
=            Смена цветы лейбла в блоке с предложениями            =
==================================================================*/

$('.carousel-offers__slide').each(function() {
  
  $(this).mouseover(function() {
    let bgColor = $(this).css('backgroundColor');
    let label = $(this).children('.carousel-offers__slide-label');

    label.css('backgroundColor', bgColor);
  });

  $(this).mouseout(function() {
    let label = $(this).children('.carousel-offers__slide-label');
    label.css('backgroundColor', '');
  })
});

/*=====  End of Смена цветы лейбла в блоке с предложениями  ======*/

$(".c-product .js-product-buy").on("click", function (e) {
  $(this).parents().children('.c-product__main').find('img')
    .clone()
    .css({
      'position': 'absolute',
      'border-radius': '50%',
      'height': '32px',
      'width': '32px',
      'z-index': '99999999',
      top: $(this).offset().top - 300,
      left: $(this).offset().left - 100
    })
    .appendTo("body")
    .animate({
      opacity: 1,
      left: $(".navbar__cart").offset()['left'],
      top: $(".navbar__cart").offset()['top'],
      width: 20
    }, 1000, function () {
      $(this).remove();
    });
});

$(".product-main .js-product-buy").on("click", function (e) {
  $(this).parents().children('.js-carousel-product-pictures').find('img')
    .clone()
    .css({
      'position': 'absolute',
      'border-radius': '50%',
      'height': '32px',
      'width': '32px',
      'z-index': '99999999',
      top: $(this).offset().top - 100,
      left: $(this).offset().left - 200
    })
    .appendTo("body")
    .animate({
      opacity: 1,
      left: $(".navbar__cart").offset()['left'],
      top: $(".navbar__cart").offset()['top'],
      width: 20
    }, 1000, function () {
      $(this).remove();
    });
});
import Swiper from "swiper";

let galleryThumbsTimeout;
let galleryNav, galleryThumbs;
let oldWidth, newWidth;

oldWidth = window.innerWidth;

galleryNavInit();

function galleryNavInit() {
    galleryNav = new Swiper('.js-main-swiper-nav', {
        spaceBetween: 0,
        direction: 'vertical',
        slidesPerView: '3',
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        centeredSlides: true,
        preloadImages: true,
        updateOnImagesReady: true,
        loop: true,
        loopedSlides: '999',
        //navigation: {
        //    nextEl: '.swiper-button-next',
        //    prevEl: '.swiper-button-prev',
        //},
        slideToClickedSlide: true,
    });

    galleryNav.on('reachBeginning', function () {
        $('.js-swiper-nav-prev').removeClass('_enabled');
    });

    galleryNav.on('reachEnd', function () {
        $('.js-swiper-nav-next').removeClass('_enabled');
    });
}

function galleryThumbsInit() {

    let galleryThumbsFade;

    if (window.innerWidth > 767) {
        galleryThumbsFade = {
            effect: 'fade',
            fadeEffect: {
                crossFade: true
            }
        }
    }

    galleryThumbs = new Swiper('.js-main-swiper-thumbs', Object.assign({
        roundLength: true,
        slidesPerView: '1',
        spaceBetween: 10,
        touchRatio: 0.2,
        loop: true,
        loopedSlides: '999',
        preloadImages: false,
        lazy: true,
        updateOnImagesReady: true,
        //autoplay: {
        //    delay: 8000,
        //    disableOnInteraction: true
        //},
        thumbs: {
            swiper: galleryNav
        },
        breakpoints: {
            767: {},
            5000: {
                allowTouchMove: false
            }
        }
    }, galleryThumbsFade));

    galleryThumbs.on('slideChange', function () {
        setMainBg(this.realIndex);
    });

    $(window).on('resize.galleryThumbs', function () {
        clearTimeout(galleryThumbsTimeout);
        galleryThumbsTimeout = setTimeout(() => {
            newWidth = window.innerWidth;
            if (oldWidth <= 767 && newWidth > 767 || oldWidth > 767 && newWidth <= 767) {
                oldWidth = newWidth;
                galleryNav.destroy();
                galleryNavInit();
                galleryThumbs.destroy();
                galleryThumbsInit();

            }

        }, 500);
    });

    $('.js-main-nav-slide').on('click', function () {
        setMainBg(+$(this).attr('data-num'));
    });
    $('.js-swiper-nav-prev').on('click', function () {
        galleryNav.slidePrev();
        $('.js-swiper-nav-next').addClass('_enabled');
    });
    $('.js-swiper-nav-next').on('click', function () {
        galleryNav.slideNext();
        $('.js-swiper-nav-prev').addClass('_enabled');
    });

    setMainBg(0);
}

let prevMod = '_light';

if ($('.js-main-swiper-thumbs').length && $('.js-main-swiper-nav').length) {
    galleryNav.destroy();
    galleryNavInit();
    galleryThumbsInit();

}

function setMainBg(index) {
    // galleryThumbs.slideTo(index);
    let mainMod = $(galleryThumbs.slides[index]).attr('data-main');

    // console.log(mainMod);
    // //TODO ГАВНО КОД

    $('.js-main-nav-slide').each(function () {
        $(this).removeClass('main-nav__slide-label--active');
        if ($(this).data('num') == index) {
            $(this).addClass('main-nav__slide-label--active');
        }
    });


    let $selector = $('.main,.navbar');
    if (mainMod) {
        if (prevMod) {
            $selector.removeClass(prevMod);
        }
        $selector.addClass(mainMod);
        prevMod = mainMod;
    }
}

$(window).scroll(function () {
    let topPosition = $(window).scrollTop();
    let $navbar = $('.navbar')
    if (topPosition <= 1) {
        $navbar.removeClass('_sticky');
        if (prevMod === '_dark') {
            $navbar.removeClass('_light').addClass('_dark');
        }
    } else if (topPosition > 1) {
        $navbar.addClass('_sticky');
        if (prevMod === '_dark') {
            $navbar.removeClass('_dark').addClass('_light');
        }
    }
});

if(galleryNav && galleryThumbs) {
    galleryThumbs.controller.control = galleryNav;
    galleryNav.controller.control = galleryThumbs;
}
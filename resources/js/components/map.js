import {initMap, addMarker} from "../modules/googleMap";
import 'images/marker.svg';

if ($('#contactsMap').length !== 0) {

    let width = document.documentElement.clientWidth, markerSize;

    const center = {lat: 55.846247, lng: 37.6382593};

    let map = initMap({
        center: center,
        target: 'contactsMap',
        zoom: 16
    });

    const marker = addMarker({
        target: map,
        position: 20,
        location: center,
        markerImg: 'assets/images/marker.svg',
        markerSize: new google.maps.Size(42, 60),
        scaledSize: new google.maps.Size(25, 25)
    });

    // let resizeEvt
    // $(window).bind('resize.gmap', function () {
    //   clearTimeout(resizeEvt);
    //   resizeEvt = setTimeout((function () {
    //     google.maps.event.trigger(map, 'resize');
    //     map.setCenter(location);
    //   }), 500);
    // });

}
import {svg} from "../utils/svg";

function formatResultPaints (state) {
    if (!state.id) {
        return state.text;
    }
    let $elem = $(state.element);
    var $state = $(
        '<span class="product-main-select-option">' +
        '<span class="product-main-select-option__size">' +  $elem.attr('data-size') + '</span>' +
        '<span class="product-main-select-option__price">' + $elem.attr('data-price') + '</span>' +
        '</span>'
    );
    return $state;
};

function formatSelectionPaints (state) {
    if (!state.id) {
        return state.text;
    }
    let $elem = $(state.element);
    var $state = $(
        '<span class="product-main-select-option">' +
        '<span class="product-main-select-option__size">' +  $elem.attr('data-size') + '</span>' +
        '<span class="product-main-select-option__price">' + $elem.attr('data-price') + '</span>' +
        svg({name: 'angle_top', mod: 'selection__angle'}) +
        '</span>'
    );
    return $state;
};

$('.js-select-paints').select2({
    minimumResultsForSearch: Infinity,
    templateSelection: formatSelectionPaints,
    templateResult: formatResultPaints,
    containerCssClass: '_cont',
    dropdownCssClass: '_drop _paints'
});

function formatResultCatalog (state) {
    if (!state.id) {
        return state.text;
    }
    let $elem = $(state.element);
    var $state = $(
        '<span class="catalog-filter-select-option">' +
        '<span class="catalog-filter-select-option__type">' +  $elem.attr('data-filter') + '</span>' +
        '</span>'
    );
    return $state;
};

function formatSelectionCatalog (state) {
    if (!state.id) {
        return state.text;
    }
    let $elem = $(state.element);
    var $state = $(
        '<span class="catalog-filter-select-option">' +
        '<span class="catalog-filter-select-option__type">' +  $elem.attr('data-filter') + '</span>' +
        svg({name: 'angle_top', mod: 'selection__angle'}) +
        '</span>'
    );
    return $state;
};

$('.js-select-catalog').select2({
    minimumResultsForSearch: Infinity,
    templateSelection: formatSelectionCatalog,
    templateResult: formatResultCatalog,
    containerCssClass: '_cont',
    dropdownCssClass: '_drop _catalog'
});

$('.select2-selection__rendered').removeAttr('title');

$('li.navbar-nav__item-wr__catalog').hover(function () {
    $(this).find('.navbar-nav__submenu').stop(true, true).delay(200).fadeIn(500);
}, function () {
    $(this).find('.navbar-nav__submenu').stop(true, true).delay(200).fadeOut(500);
});
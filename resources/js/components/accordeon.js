$('.js-acc-head').click(function() {
  let body = $(this).parents('.js-acc').find('.js-acc-body');
  if ($(this).parents('.js-acc-wrap').hasClass('_open') && body.is(':hidden')) {
    console.log('true');
    $('.js-acc-body').each(function () {
      $(this).slideUp(300).removeClass('_active');
    });
  }

  if (!body.hasClass('_active')) {
    body.slideDown(300).addClass('_active');
    $(this).parents('.js-acc-wrap').addClass('_open');
  } else {
    body.slideUp(300).removeClass('_active');
    $(this).parents('.js-acc-wrap').removeClass('_open');
  }
});
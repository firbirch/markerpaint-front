import {svg} from "../utils/svg";
import 'slick-carousel/slick/slick.min.js';
require('slick-carousel/slick/slick.css');

$('.js-carousel-offers').slick({
    arrows: false,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                dots: true
            }
        }, {
            breakpoint: 1119,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 5000,
            settings: {
                slidesToShow: 3
            }
        }
    ]
    // variableWidth: true
});

$('.js-carousel-posts').slick({
    arrows: true,
    lazyLoad: 'progressive',
    autoplaySpeed: 10000,
    nextArrow: '<button type="button" class="slick-next"><svg class="slick-arrow__icon icon-svg_s-angle_top"><use xlink:href="/assets/images/sprite.svg#s-angle_top"></use></svg></button>',
    prevArrow: '<button type="button" class="slick-prev"><svg class="slick-arrow__icon icon-svg_s-angle_top"><use xlink:href="/assets/images/sprite.svg#s-angle_top"></use></svg></button>',
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                dots: true
            }
        }, {
            breakpoint: 1119,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 5000,
            settings: {
                slidesToShow: 4
            }
        }
    ]
});

$('.js-carousel-advantages').slick({
    arrows: false,
    infinite: false,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                dots: true,
                variableWidth: true
            }
        }, {
            breakpoint: 1119,
            settings: {
                variableWidth: true
            }
        }, {
            breakpoint: 5000,
            settings: "unslick"
        }
    ]
});

$('.js-carousel-product-pictures').slick({
    arrows: false,
    infinite: true,
    dots: true,
    slidesToShow: 1
});

$('.js-carousel-product-info-nav').slick({
    responsive: [
        {
            breakpoint: 767,
            settings: {
                arrows: false,
                infinite: false,
                dots: false,
                variableWidth: true
            }
        }, {
            breakpoint: 5000,
            settings: "unslick"
        }
    ]
});
function XFormatPrice(_number) {
    let decimal = 0;
    let separator = ' ';
    let decpoint = '.';
    let format_string = '#';
    let r = parseFloat(_number);
    let exp10 = Math.pow(10, decimal); // приводим к правильному множителю
    r = Math.round(r * exp10) / exp10; // округляем до необходимого числа знаков после запятой
    let rr = Number(r).toFixed(decimal).toString().split('.');
    let b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1" + separator);
    r = (rr[1] ? b + decpoint + rr[1] : b);
    return format_string.replace('#', r);
}

$('input[name="promocode"]').keyup(function () {
    let data = $('input[name="promocode"]').val();

    // if ($(this).hasClass("js-cart__result-promo-submit--error")) {
    //     $('input[name="promocode"]').val("");
    //     $(".cart__result-promo-submit").removeClass("cart__result-promo-submit--error");
    //     $(".input-promocode-wrap").removeClass("input-promocode-wrap--error");
    //     $(".cart__result-promo-submit-text").show();
    //     $(".cart__result-promo-submit").removeClass("cart__result-promo-submit--ok");
    //     $(".cart__result-promo-submit").removeClass("js-cart__result-promo-submit--error");
    // } else if (data === "test") {
    //     $(".cart__result-promo-submit").removeClass("cart__result-promo-submit--error");
    //     $(".cart__result-promo-submit").removeClass("js-cart__result-promo-submit--error");
    //     $(".cart__result-promo-submit-text").hide();
    //     $(".cart__result-promo-submit").addClass("cart__result-promo-submit--ok");
    //     $(".input-promocode-wrap").removeClass("input-promocode-wrap--error");
        
    // } else {
    //     $(".cart__result-promo-submit").removeClass("cart__result-promo-submit--ok");
    //     $(".input-promocode-wrap").addClass("input-promocode-wrap--error");
    //     $(".cart__result-promo-submit-text").hide();
    //     $(".cart__result-promo-submit").addClass("cart__result-promo-submit--error");
    //     $(".cart__result-promo-submit").addClass("js-cart__result-promo-submit--error");
    // }
    
    $.ajax({
        url: '/cart/ActivePromo',
        data: {
            code: data
        },
        type: "POST",
        success: function (response) {
            let answer = JSON.parse(response);
            // Здесь теперь в answer приходит oldSum (если скидки не обнаружено, или ошибка = 0), иначе стоимость: 1000
            if (answer.status === "ok") {
                $(".cart__result-promo-submit").removeClass("cart__result-promo-submit--error");
                $(".cart__result-promo-submit").removeClass("js-cart__result-promo-submit--error");
                $(".cart__result-promo-submit-text").hide();
                $(".cart__result-promo-submit").addClass("cart__result-promo-submit--ok");
                $(".input-promocode-wrap").removeClass("input-promocode-wrap--error");
            } else {
                $(".cart__result-promo-submit").removeClass("cart__result-promo-submit--ok");
                $(".input-promocode-wrap").addClass("input-promocode-wrap--error");
                $(".cart__result-promo-submit-text").hide();
                $(".cart__result-promo-submit").addClass("cart__result-promo-submit--error");
                $(".cart__result-promo-submit").addClass("js-cart__result-promo-submit--error");
            }
            $('#cartCost, .cart__result-cost-sum').html(XFormatPrice(answer.cart));

        }
    });

    return false;
});
// window.cust = window.cust ? window.cust : {};
export let breakPoint = false,
	bp_XX      = 768,
	bp_XS      = 992,
	bp_SM      = 1200,
	bp_MD      = 1620,
	body       = $('body');

$(window).on('load resize', resizer);

function resizer(){
	let ww = window.innerWidth;

	if (ww < bp_XX) {windowXX();};
	if (ww >= bp_XX && ww < bp_XS) {windowXS();};
	if (ww >= bp_XS && ww < bp_SM) {windowSM();};
	if (ww >= bp_SM && ww < bp_MD) {windowMD();};
	if (ww >= bp_MD) {windowLG();};
};

function windowXX(){
	if (breakPoint != 'xx') {
		breakPoint = 'xx';
		body.trigger('resize_xx_once');
	};
	body.trigger('resize_xx');
};

function windowXS(){
	if (breakPoint != 'xs') {
		breakPoint = 'xs';
		body.trigger('resize_xs_once');
	};
	body.trigger('resize_xs');
};

function windowSM(){
	if (breakPoint != 'sm') {
		breakPoint = 'sm';
		body.trigger('resize_sm_once');
	};
	body.trigger('resize_sm');
};

function windowMD(){
	if (breakPoint != 'md') {
		breakPoint = 'md';
		body.trigger('resize_md_once');
	};
	body.trigger('resize_md');
};

function windowLG(){
	if (breakPoint != 'lg') {
		breakPoint = 'lg';
		body.trigger('resize_lg_once');
	};
	body.trigger('resize_lg');
};
resizer();
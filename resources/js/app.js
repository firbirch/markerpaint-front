// if(module.hot) {
//   module.hot.accept('./common/navbar', function () {
//     require('./common/navbar');
//   });
// }

const imagesContext = require.context('images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

window.$ = window.jQuery = require('jquery');

import {
    svg
} from './utils/svg'

import 'swiper/dist/css/swiper.min.css';
import Swiper from 'swiper';

require('./modules/browserDetect');
require('./modules/resizer');

require('select2/dist/css/select2.min.css');
require('select2/dist/js/select2.full.min');

require('perfect-scrollbar/css/perfect-scrollbar.css');
import PerfectScrollbar from 'perfect-scrollbar';

require('./bootstrap/transition');
require('./bootstrap/collapse');
require('./bootstrap/dropdown');
require('./bootstrap/tab');

require('./components/map');
require('./components/carousel');
require('./components/accordeon');
require('./components/promocode');
require('./components/calculate');
require('./components/scripts');
require('./components/modal');

require('@fancyapps/fancybox/dist/jquery.fancybox.min.css');
require('@fancyapps/fancybox/dist/jquery.fancybox.min.js');
require('./modules/cookie/index.js');
require('../sass/main.scss');

if (NODE_ENV === 'development') {
    require('../views/pages/index.twig');
    require('../views/pages/product.twig');
    require('../views/pages/contacts.twig');
    require('../views/pages/catalog.twig');
    require('../views/pages/ordering.twig');
    require('../views/pages/about.twig');
    require('../views/pages/cart.twig');
}

$(window).on('load', function () {
    require('./components/faqScroll');
});

function XFormatPrice(_number) {
    let decimal = 0;
    let separator = ' ';
    let decpoint = '.';
    let format_string = '# <span>&#8381;</span>';
    let r = parseFloat(_number);
    let exp10 = Math.pow(10, decimal); // приводим к правильному множителю
    r = Math.round(r * exp10) / exp10; // округляем до необходимого числа знаков после запятой
    let rr = Number(r).toFixed(decimal).toString().split('.');
    let b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1" + separator);
    r = (rr[1] ? b + decpoint + rr[1] : b);
    return format_string.replace('#', r);
}

(function ($) {
    $(document).ready(function () {
        require('./components/projects/modal');
        require('./components/swiper');
    });

    

    $(window).on('load', function () {

        
        require('./components/select2');

        // обработка дижения шапки лежит в ./components/swiper
        $('.radio__field').on('click', function () {


            $(this).closest('.c-product__size').find('.c-product__size-input').each(function () {
                $(this).removeClass('radio__field--active');
            });
            $(this).closest('.c-product__size-input').addClass('radio__field--active');


            //---- Смена описания в каталоге ----
            let value = $(this).closest('.c-product__size-input').data('square');
            $(this).closest('.c-product__main').find('.c-product__info').html(value);
            //---- END ----



            updateProductListCost($(this).closest('.c-product__size-input'), $(this).closest('.c-product'), $(this).parents('.c-product').find('.js-product-count-value').val());
        });

        function updateProductListCost($class, $product, $count) {
            let cost = parseInt($class.data('param_cost'));
            console.log(cost);
            $product.find('.c-product__panel-price').html(XFormatPrice(cost * $count));
        }

        function updateCart() {
            let sum = null;
            let oldSum = null;
            let productId = null;
            $('.cart__tbody tr.js-product-count-wr').each(function (index) {
                oldSum += parseInt($(this).find('.cart__tbody-cell._cost .cart-item__cost').data('cost'));

                if (oldSum >= 5000) {
                    $('.js-free-delivery').show();
                } else {
                    $('.js-free-delivery').hide();
                }
            });
            $('.cart__result-cost-sum').html(parseInt(oldSum).toLocaleString('ru'));
            $('.cart__tbody-tr').each(function () {
                if ($(this).data('product') === null) return true;
                $.cookie($(this).data('product'), 'price_' + parseInt($(this).find('.cart__tbody-cell._price .cart-item__price').data('price')) +
                    '_value_' + $(this).find('.js-product-count-value').val(), {
                        path: '/',
                        expires: 1
                    });
            });

            $.post('/cart/UpdateCart', function (response) {
                console.log('updcart ' + response);
                $('.cart__result-cost-sum').html(parseInt(response).toLocaleString('ru'));
            });

        }

        $('.cart-item__close').on('click', function () {
            $.ajax({
                url: '/cart/DeleteProduct',
                context: $(this),
                type: 'POST',
                data: {
                    productId: $(this).closest('.js-product-count-wr').data('product')
                },
                success: function (cost) {
                    $(this).closest('.cart-item').remove();
                    console.log('delete' + cost);
                    $('.cart__result-cost-sum').html(parseInt(cost).toLocaleString('ru'));
                }
            });
        });

        $('.js-product-count-button').on('click', function () {
            let $this = $(this),
                $countElem = $($this.closest('.js-product-count')).find('.js-product-count-value');
            let count = +$countElem.val();
            if ($this.hasClass('_down') && count > 0) {
                count--;
            } else if ($this.hasClass('_up')) {
                count++;
            } else {
                alert('что-то пошло не так...');
                return;
            }



            let submit = $($this.closest('.js-product-count-wr')).find('.js-product-buy');
            if (!count) {
                this.disabled = true;
                if (submit.length) {
                    submit[0].disabled = true;
                }
            } else if (count === 1 && $this.hasClass('_up')) {
                $($this.closest('.js-product-count')).find('.js-product-count-button._down')[0].disabled = false;
                if (submit.length) {
                    submit[0].disabled = false;
                }
            }
            $countElem.val(count);
            let price = parseInt($this.closest('.js-product-count-wr').find('.cart__tbody-cell._price .cart-item__price').data('price'));
            $this.closest('.js-product-count-wr').find('.cart-item__cost').each(function() {
                $(this).data('cost', count * price).html(XFormatPrice(count * price));
            })


            let price_single = parseInt($this.closest('.product-choose__item').data('price'));
            $this.closest('.product-choose__item').find('.product-choose__item-price-container').html(parseInt(price_single * count).toLocaleString('ru'));

            if(window.location.pathname != '/cart' && window.location.pathname != '/cart.html')
                updateSingleCost();

            $countElem.val(count);
            updateCart();

        });

        function updateSingleCost() {
            let price = null;
            $('.product-choose__item').each(function () {
                let priceSingle = $(this).data('price');
                price += parseInt(($(this).find('.js-product-count-value').val()) * parseInt(priceSingle));
            });
            $('.product-choose__total-price-container').html(price.toLocaleString('ru'));
        }


        $('.js-ordering-paying-radio').on('change', function () {
            $('.js-ordering-paying-content._active').removeClass('_active');
            $('.js-ordering-paying-content._' + this.value).addClass('_active');
        });

        if ($('#orderingList').length) {

            const ps = new PerfectScrollbar('#orderingList', {
                wheelSpeed: 2,
                wheelPropagation: true,
                minScrollbarLength: 20
            });
        }

        // Подсчет в карточке товара

        let catalogArr = $('.c-product');

        catalogArr.each(function () {

            let countToggle = $(this).find('.js-product-count-wr').find('.js-product-count').find('.js-product-count-button');

            countToggle.click(() => {
                let activeRadio = $(this).find('.c-product__size').find('.radio__field--active');



                let price = parseInt(activeRadio.data('param_cost'));
                let total = $(this).find('.c-product__panel-price');
                let count = $(this).find('.js-product-count-value').val();


                total.html(XFormatPrice(count * price))
            });
        });

        $('.main-nav').show();

        $('#faq-accordion').collapse({
            parent: true
        })
    })
})(jQuery);

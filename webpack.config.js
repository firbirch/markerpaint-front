const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const SpritesmithPlugin = require('webpack-spritesmith');


const devServer = {
    open: true,
    clientLogLevel: 'none',
    index: 'index.html',
    contentBase: path.resolve(__dirname, 'resources'),
};

const NODE_ENV = process.env.NODE_ENV || 'development';
const isProd = NODE_ENV === 'production';

let twigToHtml = [
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/index.twig'),
        filename: 'index.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/product.twig'),
        filename: 'product.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/contacts.twig'),
        filename: 'contacts.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/catalog.twig'),
        filename: 'catalog.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/ordering.twig'),
        filename: 'ordering.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/delivery-new.twig'),
        filename: 'delivery-new.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/faq-new.twig'),
        filename: 'faq-new.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/about.twig'),
        filename: 'about.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/cart.twig'),
        filename: 'cart.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/faq.twig'),
        filename: 'faq.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/projects.twig'),
        filename: 'projects.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/404.twig'),
        filename: '404.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'resources/views/pages/delivery.twig'),
        filename: 'delivery.html',
        chunks: ['bundle'],
        templateParameters: {
            all: require('./resources/data/all.json')
        }
    })

];

const imgRules = [
    {
        test: /\.svg$/,
        include: path.resolve(__dirname, 'resources/assets/svg'),
        use: [
            {
                loader: "file-loader",
                options: {
                    context: path.resolve(__dirname, "resources/"),
                    name(file) {
                        return isProd ? '[path][name].[ext]' : '[path][name].[hash].[ext]';
                    }
                }
            }
        ]
    },
    {
        test: /\.(jpg|png|svg)$/,
        exclude: path.resolve(__dirname, 'resources/assets/svg'),
        use: [
            {
                loader: "url-loader",
                options: {
                    context: path.resolve(__dirname, "resources/"),
                    limit: 10,
                    name(file) {
                        return isProd ? '[path][name].[ext]' : '[path][name].[hash].[ext]';
                    }
                },
            }
        ]
    }
];

const plugins = [
    ...twigToHtml,
    new webpack.DefinePlugin({
        NODE_ENV: JSON.stringify(NODE_ENV)
    }),
    new MiniCssExtractPlugin({
        filename: !isProd ? 'css/[name].[hash].css' : 'css/[name].css',
        allChunks: false
    }),
    new CleanWebpackPlugin(['public/'], {
        root: __dirname,
        verbose: true
    }),
    new SpritesmithPlugin({
        src: {
            cwd: path.resolve(__dirname, 'resources/assets/icons'),
            glob: '*.png'
        },
        target: {
            image: path.resolve(__dirname, 'resources/assets/images/sprite.png'),
            css: path.resolve(__dirname, 'resources/sass/common/_sprite.scss')
        },
        spritesmithOptions: {
            algorithm: 'diagonal',
            padding: 5,
            imgName: '../images/sprite.png',
        },
        apiOptions: {
            generateSpriteName: fullPathToSourceFile => {
                const {name} = path.parse(fullPathToSourceFile);
                return `sprite_${name}`;
            },
            cssImageRef: "../assets/images/sprite.png"
        }
    })
];

if (isProd) {
    imgRules.push({
        loader: 'image-webpack-loader',
        options: {
            bypassOnDebug: true,
            svgo: {
                plugins: [
                    {
                        cleanupIDs: false,
                        removeUselessDefs: false,
                        removeUselessStrokeAndFill: false
                    },
                    {
                      removeViewBox: false
                    }
                ]
            },
        }
    });
}

const webpackConfig = {
    entry: {
        bundle: './resources/js/app.js'
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'js/[name].js'
    },
    devServer: devServer,
    resolve: {
        alias: {
            images: path.resolve(__dirname, 'resources/assets/images/'),
            fonts: path.resolve(__dirname, 'resources/assets/fonts/'),
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['env']
                    }
                }
            },
            {
                test: /\.twig$/,
                exclude: [/node_modules/],
                use: [
                    {
                        loader: 'twig-loader'
                    }
                ]
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: isProd ? MiniCssExtractPlugin.loader : 'style-loader',
                        options: isProd ? {
                            publicPath: '../'
                        } : {}
                    },
                    {
                        loader: "css-loader",
                        options: {
                            minimize: isProd ? {discardComments: {removeAll: true}} : false,
                            sourceMap: !isProd,
                            alias: {
                                images: path.resolve(__dirname, 'resources/assets/images/')
                            }
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            plugins: [
                                autoprefixer({
                                    browsers: ['ie >= 9', 'last 4 version']
                                })
                            ],
                            sourceMap: !isProd
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {sourceMap: !isProd}
                    },

                ]
            },
            {
                test: /\.ttf|woff|woff2$/,
                loader: 'file-loader',
                options: {
                    name(file) {
                        return isProd ? 'assets/fonts/[name].[ext]' : 'assets/fonts/[name].[hash].[ext]';
                    }
                }
            }
        ].concat(imgRules)
    },
    plugins: plugins
};

module.exports = webpackConfig;
